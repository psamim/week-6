import "./App.css";
import React, { useState } from "react";
import axios from "axios";

const App = () => {
  const [name, setName] = useState("Car");
  const [images, setImages] = useState([]);

  const onInputChange = (event) => {
    setName(event.target.value);
  };

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.get(
        "https://api.unsplash.com/search/photos",
        {
          params: {
            client_id: "ADD_CLIENT_ID",
            query: this.state.name,
          },
        }
      );
      setImages(response.data.results);
    } catch (e) {
      this.setState({ error: e });
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <label>Name:</label>
      <input value={name} onChange={onInputChange} />
      <button type="submit">Submit</button>
      <p>There are {images.length} images</p>
      <div>
        {this.state.images.map((image) => (
          <img
            key={image.key}
            src={image.urls.thumb}
            alt={image.alt_description}
          />
        ))}
      </div>
    </form>
  );
};

export default App;
